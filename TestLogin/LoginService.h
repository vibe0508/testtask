//
//  LoginService.h
//  TestLogin
//
//  Created by Вячеслав Бельтюков on 18.05.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoginRequest.h"

extern NSString * const kServerErrorDomain;
extern NSString * const kClientErrorDomain;

@interface LoginService : NSObject

@property (nonatomic, readonly) BOOL isLoggedIn;
@property (nonatomic, readonly) NSDate *tokenExpirationDate;

+ (LoginService*)sharedService;
- (void)loginWithRequest:(LoginRequest*)request success:(SuccessBlock)success error:(ErrorBlock)errorBlock;

@end
