//
//  TokenResponse.h
//  TestLogin
//
//  Created by Вячеслав Бельтюков on 18.05.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "WSMessage.h"

@interface TokenResponse : WSMessage

@property (nonatomic, strong) NSString *apiToken;
@property (nonatomic, strong) NSString *expirationDateString;

@end
