//
//  TokenResponse.m
//  TestLogin
//
//  Created by Вячеслав Бельтюков on 18.05.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "TokenResponse.h"

@implementation TokenResponse

+ (NSString*)typeString{
    return @"CUSTOMER_API_TOKEN";
}

- (NSDictionary *)mapping{
    return @{@"apiToken":@"api_token",
             @"expirationDateString":@"api_token_expiration_date"};
}

@end
