//
//  WSRequest.h
//  TestLogin
//
//  Created by Вячеслав Бельтюков on 18.05.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WSMessage : NSObject

+ (instancetype)messageFromDictionary:(NSDictionary*)dict;
+ (NSString*)typeString;

@property (nonatomic, readonly) NSString *requestType;
@property (nonatomic, readonly) NSDictionary *mapping;
@property (nonatomic, readonly) NSDictionary *serialized;
@property (nonatomic) NSDictionary *data;
@property (nonatomic) NSString *sequenceID;

@end