//
//  InnerViewController.m
//  TestLogin
//
//  Created by Вячеслав Бельтюков on 18.05.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "InnerViewController.h"
#import "LoginService.h"

@interface InnerViewController ()

@property (nonatomic, weak) IBOutlet UILabel *infoLabel;

@end

@implementation InnerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.infoLabel.text = [NSString stringWithFormat:@"Your token will expire at %@", [LoginService sharedService].tokenExpirationDate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIModalTransitionStyle)modalTransitionStyle{
    return UIModalTransitionStyleCrossDissolve;
}

@end
