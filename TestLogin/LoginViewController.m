//
//  LoginViewController.m
//  TestLogin
//
//  Created by Вячеслав Бельтюков on 18.05.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "LoginViewController.h"
#import "FormCell.h"
#import "FormCellModel.h"
#import "LoginService.h"
#import "InnerViewController.h"

static NSTimeInterval kAnimationDuration = 0.5;

@interface LoginViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *formView;
@property (nonatomic, weak) IBOutlet UILabel *errorLabel;
@property (nonatomic, weak) IBOutlet UIButton *loginButton;

@property (nonatomic, strong) NSArray *cellModels;
@property (nonatomic, strong) LoginRequest *request;

- (IBAction)done;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.request = [LoginRequest new];
    [self.formView registerNib:[UINib nibWithNibName:@"FormCell" bundle:nil] forCellReuseIdentifier:@"formCell"];
    self.cellModels = @[@[[FormCellModel modelOfType:FormCellEmail valueOwner:self.request andKey:@"email"],
                          [FormCellModel modelOfType:FormCellPassword valueOwner:self.request andKey:@"password"]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.cellModels.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.cellModels[section] count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FormCellModel *model = self.cellModels[indexPath.section][indexPath.row];
    if (indexPath.row == [self tableView:tableView numberOfRowsInSection:indexPath.section] - 1) {
        model.returnKeyType = UIReturnKeyDone;
    }
    FormCell *cell = [tableView dequeueReusableCellWithIdentifier:model.cellIdentifier forIndexPath:indexPath];
    cell.model = model;
    __weak typeof(self) weakSelf = self;
    cell.nextBlock = ^{
        if ([self.formView numberOfRowsInSection:indexPath.section] - 1 == indexPath.row) {
            [weakSelf done];
        }else{
            [weakSelf.formView selectRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row+1 inSection:indexPath.section]
                                       animated:YES scrollPosition:UITableViewScrollPositionTop];
        }
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

- (void)done{
    __weak typeof(self) weakSelf = self;
    self.loginButton.enabled = NO;
    [[LoginService sharedService] loginWithRequest:self.request success:^{
        [weakSelf loginSuccess];
    } error:^(NSError *error) {
        [weakSelf loginError:error];
    }];
}

- (void)loginSuccess{
    [self presentViewController:[[InnerViewController alloc] init] animated:YES completion:nil];
}

- (void)loginError:(NSError*)error{
    [self displayErrorText:error.localizedDescription];
    self.loginButton.enabled = YES;
}

- (void)displayErrorText:(NSString*)text{
    self.errorLabel.text = text;
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:kAnimationDuration animations:^{
        weakSelf.errorLabel.alpha = 1.0;
    }];
}

- (void)hideErrorText{
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:kAnimationDuration animations:^{
        weakSelf.errorLabel.alpha = 0.0;
    }];
}

@end
