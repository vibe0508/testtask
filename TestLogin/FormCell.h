//
//  FormCell.h
//  TestLogin
//
//  Created by Вячеслав Бельтюков on 18.05.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FormCellModel.h"

@interface FormCell : UITableViewCell

@property (nonatomic, strong) FormCellModel *model;
@property (nonatomic, strong) SuccessBlock nextBlock;

@end
