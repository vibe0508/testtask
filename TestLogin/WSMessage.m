//
//  WSRequest.m
//  TestLogin
//
//  Created by Вячеслав Бельтюков on 18.05.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "WSMessage.h"
#import "TokenResponse.h"
#import "LoginRequest.h"
#import "ErrorResponse.h"

@implementation WSMessage

+ (NSDictionary*)messageClasses{
    static NSDictionary *classes = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        classes = @{[TokenResponse typeString] : [TokenResponse class],
                    [ErrorResponse typeString] : [ErrorResponse class]};
    });
    return classes;
}

+ (instancetype)messageFromDictionary:(NSDictionary *)dict{
    NSString *type = dict[@"type"];
    Class messageClass = [self messageClasses][type];
    WSMessage *message = [messageClass new];
    message.sequenceID = dict[@"sequence_id"];
    message.data = dict[@"data"];
    return message;
}

- (void)setData:(NSDictionary *)data{
    for (NSString *key in self.mapping.allKeys) {
        if (data[self.mapping[key]]) {
            [self setValue:data[self.mapping[key]] forKey:key];
        }
    }
}

- (NSDictionary*)data{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    for (NSString *key in self.mapping.allKeys) {
        id value = [self valueForKey:key] ?: [NSNull null];
        if ([value isKindOfClass:[WSMessage class]]) {
            value = [value serialized];
        }
        data[self.mapping[key]] = value;
    }
    return data;
}

- (NSDictionary*)serialized{
    return @{@"type": self.requestType,
             @"data": self.data};
}

- (NSString *)requestType{
    return [self.class typeString];
}

@end
