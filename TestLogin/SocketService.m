//
//  SocketService.m
//  TestLogin
//
//  Created by Вячеслав Бельтюков on 18.05.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "SocketService.h"
#import <SocketRocket/SRWebSocket.h>
#import <UIKit/UIApplication.h>

NSString * const kServerPath = @"ws://52.29.182.220:8080/customer-gateway/customer";

@interface SocketService ()<SRWebSocketDelegate>

@property (nonatomic, strong) NSMutableDictionary *subscribers;
@property (nonatomic, strong) NSMutableArray *messagesQueue;

@property (nonatomic, strong) SRWebSocket *socket;

@end

@implementation SocketService

+ (SocketService*)sharedService{
    static SocketService *sharedService = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedService = [SocketService new];
    });
    return sharedService;
}

- (instancetype)init{
    if (self = [super init]) {
        _socket = [[SRWebSocket alloc] initWithURL:[NSURL URLWithString:kServerPath]];
        _socket.delegate = self;
        _messagesQueue = [NSMutableArray array];
        _subscribers = [NSMutableDictionary dictionary];
        [_socket open];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(closeSocket)
                                                     name:UIApplicationWillTerminateNotification
                                                   object:nil];
    }
    return self;
}

- (void)webSocketDidOpen:(SRWebSocket *)webSocket{
    for (WSMessage *message in self.messagesQueue) {
        [self sendMessage:message];
    }
    [self.messagesQueue removeAllObjects];
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)data{
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data
                                                         options:0
                                                           error:nil];
    WSMessage *message = [WSMessage messageFromDictionary:dict];
    for (id<MessageSubscriber> subscriber in self.subscribers[NSStringFromClass([message class])]) {
        [subscriber receivedMessage:message];
    }
}

- (BOOL)webSocketShouldConvertTextFrameToString:(SRWebSocket *)webSocket{
    return NO;
}

- (void)closeSocket{
    [self.socket close];
}

- (void)addSubscriber:(id<MessageSubscriber>)subscriber forMessageClass:(Class)messageClass{
    NSMutableSet *subscribers = self.subscribers[NSStringFromClass(messageClass)];
    if (!subscribers) {
        subscribers = [NSMutableSet set];
        self.subscribers[NSStringFromClass(messageClass)] = subscribers;
    }
    [subscribers addObject:subscriber];
}

- (void)postMessage:(WSMessage *)message{
    if (self.socket.readyState != SR_OPEN) {
        [self.messagesQueue addObject:message];
        return;
    }
    [self sendMessage:message];
}

- (void)sendMessage:(WSMessage*)message{
    NSString *stringToSend = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:message.serialized
                                                                                            options:0
                                                                                              error:nil] encoding:NSUTF8StringEncoding];
    NSLog(@"Sent message: %@", stringToSend);
    [self.socket send:stringToSend];
}

@end
