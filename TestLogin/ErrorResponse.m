//
//  ErrorResponse.m
//  TestLogin
//
//  Created by Вячеслав Бельтюков on 18.05.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "ErrorResponse.h"

@implementation ErrorResponse

+ (NSString*)typeString{
    return @"CUSTOMER_ERROR";
}

- (NSDictionary *)mapping{
    return @{@"errorDescription":@"error_description",
             @"errorCode":@"error_code",
             @"source":@"in_event"};
}

@end