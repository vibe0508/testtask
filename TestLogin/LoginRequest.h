//
//  LoginRequest.h
//  TestLogin
//
//  Created by Вячеслав Бельтюков on 18.05.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "WSMessage.h"

@interface LoginRequest : WSMessage

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *password;

@end
