//
//  FormCellModel.h
//  TestLogin
//
//  Created by Вячеслав Бельтюков on 18.05.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

@import UIKit;

typedef enum : NSUInteger {
    FormCellEmail,
    FormCellPassword
} FormCellType;

@interface FormCellModel : NSObject

+ (instancetype)modelOfType:(FormCellType)type valueOwner:(id)valueOwner andKey:(NSString*)valueKey;

@property (nonatomic) FormCellType type;
@property (nonatomic, readonly) NSString *cellIdentifier;
@property (nonatomic, strong) NSString *placeholder;
@property (nonatomic, weak) id valueOwner;
@property (nonatomic, strong) NSString *valueKey;
@property (nonatomic) UIReturnKeyType returnKeyType;

@end
