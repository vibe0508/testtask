//
//  LoginService.m
//  TestLogin
//
//  Created by Вячеслав Бельтюков on 18.05.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "LoginService.h"
#import "SocketService.h"
#import "TokenResponse.h"
#import "ErrorResponse.h"
#import <FXKeychain/FXKeychain.h>

NSString * const kServerErrorDomain = @"server";
NSString * const kClientErrorDomain = @"client";

NSString * const kTokenStoreKey = @"token";
NSString * const kTokenDateStoreKey = @"tokenDate";

BOOL IsValidEmail(NSString *checkString) {
    NSString *filterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", filterString];
    return [emailTest evaluateWithObject:checkString];
}

@interface LoginService ()<MessageSubscriber>

@property (nonatomic, strong) SuccessBlock successBlock;
@property (nonatomic, strong) ErrorBlock errorBlock;
@property (nonatomic, assign) BOOL loginInProgress;

@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong, readwrite) NSDate *tokenExpirationDate;
@end

@implementation LoginService

static NSDateFormatter *dateFormatter = nil;

+ (LoginService*)sharedService{
    static LoginService *service = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [self new];
        dateFormatter = [NSDateFormatter new];
        [dateFormatter setLocale: [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
        dateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation: @"UTC"];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    });
    return service;
}

- (instancetype)init{
    if (self = [super init]) {
        [[SocketService sharedService] addSubscriber:self
                                     forMessageClass:[TokenResponse class]];
        [[SocketService sharedService] addSubscriber:self
                                     forMessageClass:[ErrorResponse class]];
        _token = [FXKeychain defaultKeychain][kTokenStoreKey];
        _tokenExpirationDate = [FXKeychain defaultKeychain][kTokenDateStoreKey];
    }
    return self;
}

- (void)loginWithRequest:(LoginRequest*)request success:(SuccessBlock)success error:(ErrorBlock)errorBlock{
    if (self.loginInProgress) {
        return;
    }
    self.loginInProgress = YES;
    if (!IsValidEmail(request.email)) {
        NSError *error = [NSError errorWithDomain:kClientErrorDomain
                                             code:0
                                         userInfo:@{NSLocalizedDescriptionKey : @"Your email is invalid"}];
        errorBlock(error);
        self.loginInProgress = NO;
        return;
    }
    self.successBlock = success;
    self.errorBlock = errorBlock;
    [[SocketService sharedService] postMessage:request];
}

- (void)receivedMessage:(WSMessage *)message{
    if ([message isKindOfClass:[ErrorResponse class]]) {
        [self processErrorResponse:(ErrorResponse*)message];
    }else if ([message isKindOfClass:[TokenResponse class]]) {
        [self processTokenResponse:(TokenResponse*)message];
    }
    self.loginInProgress = NO;
    self.successBlock = nil;
    self.errorBlock = nil;
}

- (void)processErrorResponse:(ErrorResponse*)response{
    NSError *error = [NSError errorWithDomain:kServerErrorDomain
                                         code:0
                                     userInfo:@{NSLocalizedDescriptionKey : response.errorDescription}];
    self.errorBlock(error);
}

- (void)processTokenResponse:(TokenResponse*)response{
    self.token = response.apiToken;
    self.tokenExpirationDate = [dateFormatter dateFromString:response.expirationDateString];
    [FXKeychain defaultKeychain][kTokenStoreKey] = self.token;
    [FXKeychain defaultKeychain][kTokenDateStoreKey] = self.tokenExpirationDate;
    self.successBlock();
}

- (BOOL)isLoggedIn{
    return self.token && [self.tokenExpirationDate compare:[NSDate date]] == NSOrderedDescending;
}

@end
