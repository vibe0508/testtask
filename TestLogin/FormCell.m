//
//  FormCell.m
//  TestLogin
//
//  Created by Вячеслав Бельтюков on 18.05.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "FormCell.h"

@interface FormCell ()<UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField *field;

@end

@implementation FormCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    if (selected) {
        [self.field becomeFirstResponder];
    }else{
        [self.field resignFirstResponder];
    }
}

- (void)setModel:(FormCellModel *)model{
    _model = model;
    self.field.text = [model.valueOwner valueForKey:model.valueKey];
    self.field.placeholder = model.placeholder;
    self.field.returnKeyType = model.returnKeyType;
    switch (model.type) {
        case FormCellEmail:
            self.field.secureTextEntry = NO;
            self.field.autocapitalizationType = UITextAutocapitalizationTypeNone;
            self.field.autocorrectionType = UITextAutocorrectionTypeNo;
            self.field.keyboardType = UIKeyboardTypeEmailAddress;
            break;
            
        case FormCellPassword:
            self.field.secureTextEntry = YES;
            self.field.keyboardType = UIKeyboardTypeDefault;
            break;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (self.nextBlock) {
        self.nextBlock();
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (range.location + range.length > textField.text.length) {
        return NO;
    }
    NSString *result = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self.model.valueOwner setValue:result forKey:self.model.valueKey];
    return YES;
}

@end
