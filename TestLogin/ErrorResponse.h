//
//  ErrorResponse.h
//  TestLogin
//
//  Created by Вячеслав Бельтюков on 18.05.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "WSMessage.h"

@interface ErrorResponse : WSMessage

@property (nonatomic, strong) NSString *errorDescription;
@property (nonatomic, strong) NSString *errorCode;
@property (nonatomic, strong) WSMessage *source;

@end
