//
//  FormCellModel.m
//  TestLogin
//
//  Created by Вячеслав Бельтюков on 18.05.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "FormCellModel.h"

@implementation FormCellModel

+ (instancetype)modelOfType:(FormCellType)type valueOwner:(id)valueOwner andKey:(NSString *)valueKey{
    FormCellModel *model = [self new];
    model.type = type;
    model.placeholder = type == FormCellEmail ? @"Email..." : @"Пароль...";
    model.valueOwner = valueOwner;
    model.valueKey = valueKey;
    model.returnKeyType = UIReturnKeyNext;
    return model;
}

- (NSString*)cellIdentifier{
    return @"formCell";
}

@end
