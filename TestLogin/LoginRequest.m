//
//  LoginRequest.m
//  TestLogin
//
//  Created by Вячеслав Бельтюков on 18.05.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import "LoginRequest.h"

@implementation LoginRequest

+ (NSString*)typeString{
    return @"LOGIN_CUSTOMER";
}

- (NSDictionary *)mapping{
    return @{@"email":@"email",
             @"password":@"password"};
}

@end
