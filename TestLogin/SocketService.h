//
//  SocketService.h
//  TestLogin
//
//  Created by Вячеслав Бельтюков on 18.05.16.
//  Copyright © 2016 Вячеслав Бельтюков. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSMessage.h"

@protocol MessageSubscriber <NSObject>

- (void)receivedMessage:(WSMessage*)message;

@end

@interface SocketService : NSObject

+ (SocketService*)sharedService;

- (void)postMessage:(WSMessage*)message;
- (void)addSubscriber:(id<MessageSubscriber>)subscriber forMessageClass:(Class)messageClass;

@end
